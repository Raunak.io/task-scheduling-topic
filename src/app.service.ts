import { Injectable, Logger } from '@nestjs/common';
import {
  Cron,
  CronExpression,
  Interval,
  Timeout,
  SchedulerRegistry,
} from '@nestjs/schedule'; //

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }

  constructor(private scheduleRegistry: SchedulerRegistry) {}
  @Cron('2 * * * * *', {
    name: 'notification',
  })
  triggerNotification() {
    const job = this.scheduleRegistry.getCronJob('notification');
    job.stop();
    console.log(job.lastDate()); //string representation of last date of job executed
  }

  private readonly logger = new Logger(AppService.name);
  @Cron('45 * * * * *')
  // @Cron(new Date(Date.now( )+10*1000)) // starts after 10 seconds when app starts
  // @Corn('* */30 9-17 * * *') every 30th min between 9 am to 5pm
  // @Corn('0 30 11 * * 1-5') mon to fri every 11:30 am
  // @Corn(CornExpression.EVERY_10_SECONDS) EVERY 10 SECOND
  handleCron() {
    this.logger.debug('automatically called at exact every 45th second');
  }

  @Interval(10000)
  handleInterval() {
    this.logger.debug('calling every 10 second');
  }

  @Timeout(5000)
  // @Timeout('reminder',5000) declarative one
  handleTimeout() {
    this.logger.debug('called once after 5 seconds');
  }

deleteCron(name:string){
  this.scheduleRegistry.deleteCronJob(name);
  this.logger.warn(`job ${name} deleted`); // to delete named cron jobs
}


}




